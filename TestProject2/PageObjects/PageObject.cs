﻿using OpenQA.Selenium;

namespace TestProject2.PageObjects
{

    public class PageObject
    {
        private readonly string BaseUrl = "https://testing-angular-applications.github.io/";
        private readonly IWebDriver driver;

        public PageObject(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void Navigate()
        {
            Navigate(BaseUrl);
        }
        public void Navigate (string url)
        {
            driver.Navigate().GoToUrl(url);
        }
    }
}
