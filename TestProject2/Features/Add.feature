﻿Feature: Add
Background: 
	Given I open the application
	And I click Add button
Scenario Outline: Should be able to enter values
	When I enter <name>, <e_mail>, <phone>
	Then <name>, <e_mail>, <phone> should be displayed

	Examples: 
	| name | e_mail     | phone      |
	| 111  | 111@111.11 | 1111111111 |
	| 222  | 222@222.22 | 2222222222 |