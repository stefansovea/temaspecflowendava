﻿using TestProject2.PageObjects;

namespace TestProject2.Utilities
{
    class Utilities_Functions
    {
        private delegate void my_delegate(string name);

        public void EnterText(string name, string e_mail, string phone, AddPo addPo)
        {
            addPo.Name.SendKeys(name);
            addPo.E_mail.SendKeys(e_mail);
            addPo.Phone.SendKeys(phone);
        }

        public bool GetValueFrom()
        {
            return true;
            //Pentru a furniza numele proprietatii pe care trebuie sa o obtina trebuie
            //sa folosim delegates. Din pacate, nu am reusit sa incapsulez corect in delegate
            //deoarece nu am putut crea o instanta a clasei AddPo fara sa ma folosesc de driver
            //si de clasa asociata
        }
    }
}
