﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace TestProject2.Drivers
{
    class DriverFactory
    {
        private readonly string browser = "CHROME";

        public IWebDriver CreateDriver()
        {
            switch (browser.ToUpperInvariant())
            {
                case "CHROME":
                    return new ChromeDriver();
                default:
                    throw new NotSupportedException($"Browser {browser} is not supported");
            }
        }
    }
}
