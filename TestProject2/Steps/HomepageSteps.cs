﻿using FluentAssertions;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using TestProject2.PageObjects;

namespace TestProject2.Steps
{
    [Binding]
    public class HomepageSteps
    {
        private readonly HomepagePo homepagePo;

        public HomepageSteps(IWebDriver driver)
        {
            homepagePo = new HomepagePo(driver);
        }

        [Given(@"I open the application")]
        [When(@"I open the application")]
        public void WhenIOpenTheApplication()
        {
            homepagePo.Navigate();
        }

        [Given(@"I click Add button")]
        public void GivenIClickAddButton()
        {
            homepagePo.AddButton.Click();
        }

        [Then(@"Add Button should be displayed")]
        public void ThenAddButtonShouldBeDisplayed()
        {
            homepagePo.AddButton.Displayed.Should().BeTrue("Add button is not displayed");
        }
    }
}
