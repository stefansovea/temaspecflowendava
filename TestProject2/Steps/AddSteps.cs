﻿using FluentAssertions;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using TestProject2.PageObjects;
using TestProject2.Utilities;

namespace TestProject2.Steps
{
    [Binding]
    public class AddSteps
    {
        private readonly AddPo addPo;
        Utilities_Functions utilities_Functions;

        public AddSteps(IWebDriver driver)
        {
            addPo = new AddPo(driver);
        }

        [When(@"I enter (.*), (.*), (.*)")]
        public void WhenIEnter_(string name,  string e_mail, string phone)
        {

            addPo.Name.SendKeys(name);
            addPo.E_mail.SendKeys(e_mail);
            addPo.Phone.SendKeys(phone);

           // utilities_Functions.EnterText(name, e_mail, phone, addPo);
        }
        
        [Then(@"(.*), (.*), (.*) should be displayed")]
        public void Then_ShouldBeDisplayed(string name, string e_mail, string phone)
        {
            string actualName = addPo.Name.GetAttribute("value");
            string actualE_mail = addPo.E_mail.GetAttribute("value");
            string actualPhone = addPo.Phone.GetAttribute("value");

            actualName.Should().Be(name);
            actualE_mail.Should().Be(e_mail);
            actualPhone.Should().Be(phone);
        }
    }
}
